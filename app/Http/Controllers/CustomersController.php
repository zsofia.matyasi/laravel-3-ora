<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;

class CustomersController extends Controller
{
  public function show($customerId)
  {
    $customer = Customer::find($customerId);
    //$customer = Customer::where('id', $customerId)->first();

    return view('frontend.customers.show')->with('customer', $customer);
  }

  public function index()
  {
    $customers = Customer::all();

    return view('frontend.customers.index')->with('customers', $customers);
  }

  public function create()
  {
    return view('frontend.customers.create');
  }

  public function store(Request $request)
  {
    $this->validate($request, [
      'name' => 'required',
      'email' => 'required|email|unique:customers,email',
      'password' => 'required|confirmed',
      'terms' => 'accepted'
    ]);

    $customer = new Customer;
    $customer->name = $request->input('name');
    $customer->email = $request->input('email');
    $customer->password = \Hash::make($request->input('password'));
    $customer->save();

    session()->flash('message', 'Köszönjük a regisztrációt');
    return redirect()->back();
  }

  public function edit($customerId)
  {
    $customer = Customer::find($customerId);
    return view('frontend.customers.edit')->with('customer', $customer);
  }

  public function update(Request $request, $customerId){

    $customer = Customer::find($customerId);
    $customer->name = $request->input('name');
    $customer->email = $request->input('email');
    $customer->save();

    return redirect('/lista');
  }


  /*  public function index()  //lista
    public function create() //létrehozás megjelenítése
    public function store() //adatbázisba mentés

    public function edit($id) //form megjelenítése
    public function update($id) //adatbázis művelet
    public function delete($id)
    public function show($id)*/


/*  public function index()
  {
    //1es id-ju customer
    $customer = Customer::find(1);

    dd($customer->lastUpdated());


    $customers = Customer::all();
    dd($customers);
  }*/

    public function newCustomer()
    {
      $customer = new Customer;
      $customer->name = 'Géza';
      $customer->email = 'gejza@cim.hu';
      $customer->password = \Hash::make('1234');

      $customer->save();


      dd($customer);
    }

}
