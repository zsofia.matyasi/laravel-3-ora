<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //public $table = 'customer_data';


    public function lastUpdated()
    {
      return $this->updated_at->format('Y-m-d');
    }


}
