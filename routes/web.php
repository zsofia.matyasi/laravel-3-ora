<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PagesController@index')->name('index');

Route::get('/regisztracio', 'CustomersController@create')->name('customers.create');
Route::post('/regisztracio', 'CustomersController@store')->name('customers.store');


#edit customer
Route::get('/modositas/{id}', 'CustomersController@edit')->name('customers.edit');
Route::post('/modositas/{id}', 'CustomersController@update')->name('customers.update');


Route::get('/ugyfelek', 'CustomersController@index')->name('customers.index');
Route::get('/ugyfel/{id}', 'CustomersController@show')->name('customers.show');





Route::post('/reg', 'PagesController@register')->name('register');

Route::get('/lista', 'CustomersController@index')->name('customers.index');
Route::get('/customer', 'CustomersController@newCustomer')->name('customers.newCustomer');


Route::get('/{page}', 'PagesController@show')->name('pages.show');
