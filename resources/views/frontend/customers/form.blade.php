<form action="{{route('customers.store')}}" method="POST">
  @csrf
  Név:
  <input type="text" name="name" value="{{old('name')}}">
  @if($errors->first('name'))
    <p style="color:red;">
      {{$errors->first('name')}}
    </p>
  @endif
  Email:
  <input type="text" name="email" value="{{old('email')}}">
  @if($errors->first('email'))
    <p style="color:red;">
      {{$errors->first('email')}}
    </p>
  @endif
  <br>
  Jelszó:
  <input type="password" name="password">
  <br>
  Jelszó mégegyszer:
  <input type="password" name="password_confirmation">
  @if($errors->first('password'))
    <p style="color:red;">
      {{$errors->first('password')}}
    </p>
  @endif
  <br>
  <label>
    <input type="checkbox" name="terms" value="1" {{old('terms') ? 'checked' : ''}}>
    Elfogadok mindent
  </label>
  <input type="submit" value="Register">
</form>
