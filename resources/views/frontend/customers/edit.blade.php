@extends('frontend.layout.application')

@section('content')
<br><br>
<form action="{{route('customers.update', $customer->id)}}" method="POST">
  @csrf
  Név:
  <input type="text" name="name" value="{{$customer->name}}">
  @if($errors->first('name'))
    <p style="color:red;">
      {{$errors->first('name')}}
    </p>
  @endif
  Email:
  <input type="text" name="email" value="{{$customer->email}}">
  @if($errors->first('email'))
    <p style="color:red;">
      {{$errors->first('email')}}
    </p>
  @endif
 <!--  <br>
  Jelszó:
  <input type="password" name="password" value="{{$customer->password}}">
  <br>
  Jelszó mégegyszer:
  <input type="password" name="password_confirmation" value="{{$customer->password}}">
  @if($errors->first('password'))
    <p style="color:red;">
      {{$errors->first('password')}}
    </p>
  @endif -->
  <br><br>
  <input type="submit" value="Update">
</form> 

@stop