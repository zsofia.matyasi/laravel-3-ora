@extends('frontend.layout.application')

@section('content')
  <h4>Ügyfelek</h4>
  <table border="1">
    <tr>
      <th>Id</th>
      <th>Név</th>
      <th>Email</th>
      <th>Módosítás dátuma</th>
      <th></th>
      <th></th>
    </tr>
    @foreach($customers as $customer)
      <tr>
        <td>{{$customer->id}}</td>
        <td>{{$customer->name}}</td>
        <td>{{$customer->email}}</td>
        <td>{{$customer->lastUpdated()}}</td>
        <td><a href="{{route('customers.show', ['id' => $customer->id])}}">Megtekintés</a></td>
        <td><a href="{{route('customers.edit', ['id' => $customer->id])}}">Módosítás</a></td>
      </tr>
    @endforeach
  </table>
@endsection{{-- @stop--}}
